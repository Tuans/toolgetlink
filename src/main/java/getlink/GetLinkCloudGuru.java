package getlink;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.CloudGuruUtils;
import utils.Constants;

public class GetLinkCloudGuru {
	CloudGuruUtils cloudGuruUtils;
	WebDriver driver;
	WebDriverWait driverWait;
	Actions actions;

	public void initial() {
		driver = new ChromeDriver();
		driverWait = new WebDriverWait(driver, Constants.DEFAULT_TIMEOUT);
		actions = new Actions(driver);
		cloudGuruUtils = new CloudGuruUtils(driver, driverWait, actions);
	}

	public void getlink() {
		initial();
		try {
			for (String courseURL : cloudGuruUtils.getListCourseURL(Constants.LIST_COURSE_URL_FILE_NAME)) {
				cloudGuruUtils.doGetLink(courseURL);
				destroy();
				initial();
			}
		} finally {
			destroy();
		}

	}

	public void destroy() {
		if (driver != null) {
			driver.quit();
		}
	}

	public static void main(String[] args) {
		new GetLinkCloudGuru().getlink();
	}
}
