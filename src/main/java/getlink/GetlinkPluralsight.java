package getlink;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Constants;
import utils.PluralsightUtils;

public class GetlinkPluralsight {
	PluralsightUtils pluralsightUtils;
	WebDriver driver;
	WebDriverWait driverWait;
	Actions actions;

	public void initial() {
		driver = new ChromeDriver();
		driverWait = new WebDriverWait(driver, Constants.DEFAULT_TIMEOUT);
		actions = new Actions(driver);
		pluralsightUtils = new PluralsightUtils(driver, driverWait, actions);
	}

	public void getlink(){
		initial();
		for (String courseURL : pluralsightUtils.getListCourseURL(Constants.LIST_COURSE_URL_FILE_NAME)) {
			pluralsightUtils.doGetLink(courseURL);
			destroy();
			initial();
		}
		destroy();
	}
	
	public void destroy() {
		if (driver != null) {
			driver.quit();
		}
	}
	
	public static void main(String[] args) {
		new GetlinkPluralsight().getlink();
	}
}
