package utils;

import static utils.Constants.PASSWORD;
import static utils.Constants.CLOUD_GURU_USER_NAME;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CloudGuruUtils extends BasePage{
	public static class MyRunnable extends BasePage implements Runnable {
		public MyRunnable(WebDriver driver, WebDriverWait driverWait, Actions actions) {
			super(driver, driverWait, actions);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void run() {
			sleep(60);
			By btnPauseLocator = By.xpath(".//button[@class='iconButton pause']");
			myClick(btnPauseLocator);
		}
	}

	public CloudGuruUtils(WebDriver driver, WebDriverWait driverWait,
			Actions actions) {
		super(driver, driverWait, actions);
		// TODO Auto-generated constructor stub
	}
	
	public void doLogin() {
		By btnSigninLocator = By.xpath(".//button[@id='existingAccountButton']");
		By emailLocator = By.xpath(".//input[@class='auth0-lock-input'][@name='email']");
		By passwordLocator = By.xpath(".//input[@class='auth0-lock-input'][@name='password']");
		By subbmit = By.xpath(".//button[@class='auth0-lock-submit']");

		myClick(btnSigninLocator);
		sleep(3);
		mySendKey(emailLocator, CLOUD_GURU_USER_NAME);
		mySendKey(passwordLocator, PASSWORD);
		myClick(subbmit);
		sleep(15);
	}

	public void gotoCourse() {
		myClick(By.xpath(".//div[@id='1c1']/div[1]/i[1]"));
//		myClick(By.xpath(".//div[@id='6c2']/div[1]/i[1]"));
		sleep(10);
		myClick(By.xpath(".//i[@class='mdi-navigation-chevron-right second-chevron']"));
	}
	
	public void download() {
		By btnDownload = By
				.xpath(".//i[@class='i-20 mdi-file-cloud-download']");
		myClick(btnDownload);
		sleep(2);
	}
	public void getVideoUrlAndDownload(int index) {

		
		String courseName = myGetText(By.xpath(".//div[@id='content']/div/div/div/div[2]/div[1]/div[2]/h1"));
		String subFolder = myGetText(By
				.xpath(".//div[@class='course-viewer-component-pane-info-title-table-text']"));
		String clipTitle = subFolder;
		
		String videoName = index
				+ ". "
				+ clipTitle.replaceAll("[^a-zA-Z0-9.-]", "_") + ".mp4";
		if(null == clipTitle || clipTitle.contains("Quiz") || clipTitle.contains("quiz")){
			System.err.println("courseName: " + courseName);
			return;
		}
		WebElement videotag = waiUtilVisible(By
				.xpath(".//video"));
		String srcVideo = videotag.getAttribute("src");

		System.out.println("coursename: " + courseName + ", subFolder: "
				+ subFolder + ", videoName: " + videoName + ", srcVideo: "
				+ srcVideo);
		String savePath = Constants.ROOT_PATH + courseName;
		System.out.println("savePath: " + savePath);
		ExecutorService executor = Executors.newFixedThreadPool(30);
		Runnable worker = new MyRunnable(driver, driverWait, actions);
		executor.execute(worker);
		try {
			downloadFile(srcVideo, savePath, videoName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		executor.shutdown();
		while (!executor.isTerminated()) {
			 
		}
	}
	public static void downloadFile(String fileURL, String saveDir,
			String fileName) throws IOException {
		URL url = new URL(fileURL);
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		int responseCode = httpConn.getResponseCode();

		// always check HTTP response code first
		if (responseCode == HttpURLConnection.HTTP_OK) {
			String contentType = httpConn.getContentType();
			int contentLength = httpConn.getContentLength();
			System.out.println("Content-Type = " + contentType);
			System.out.println("fileName = " + fileName);
			System.out.println("Content-Length = " + contentLength);
			File directory = new File(String.valueOf(saveDir));
			if (!directory.exists()) {
				directory.mkdirs();
			}
			// opens input stream from the HTTP connection
			InputStream inputStream = httpConn.getInputStream();
			String saveFilePath = saveDir + File.separator + fileName;

			// opens an output stream to save into file
			FileOutputStream outputStream = new FileOutputStream(saveFilePath);
			int bytesRead = -1;
			byte[] buffer = new byte[1024 * 1024];
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			outputStream.close();
			inputStream.close();
			System.out.println("File downloaded");
		} else {
			System.out
					.println("No file to download. Server replied HTTP code: "
							+ responseCode);
		}
		httpConn.disconnect();
	}

	public void doGetLink(String courseURL) {
		driver.get(courseURL);
		// driver.manage().window().maximize();
		doLogin();
		gotoCourse();
		int index = 0;
		By btnNextLocator = By.xpath(".//div[@id='content']/div/div/div/div[2]/div[3]/div[1]/a[2]/i");
		sleep(2);
		
		getVideoUrlAndDownload(index);
		while (true) {
			index++;
			myClick(btnNextLocator);
			sleep(5);
			getVideoUrlAndDownload(index);
		}
	}
	public List<String> getListCourseURL(String fileName) {
		List<String> listCourseURL = new ArrayList<String>();
		// Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				listCourseURL.add(line);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listCourseURL;
	}
}
