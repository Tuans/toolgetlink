package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	WebDriver driver;
	WebDriverWait driverWait;
	Actions actions;

	public BasePage(WebDriver driver, WebDriverWait driverWait, Actions actions) {
		this.driver = driver;
		this.driverWait = driverWait;
		this.actions = actions;
	}

	public void mySendKey(By elementLocator, String message) {
		WebElement element = waiUtilVisible(elementLocator);
		element.sendKeys(message);
	}

	public WebElement waiUtilVisible(By elementLocator) {
		WebElement element = driverWait.until(ExpectedConditions
				.presenceOfElementLocated(elementLocator));
		return element;
	}
	
	public void myClick(By elementLocator) {
		WebElement element = waiUtilVisible(elementLocator);
		actions.moveToElement(element).click().build().perform();
	}
	
	public String myGetText(By elementLocator) {
		WebElement element = waiUtilVisible(elementLocator);
		actions.moveToElement(element).build().perform();
		return element.getText();
	}
	public static void sleep(int second) {
		try {
			Thread.sleep(second * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
