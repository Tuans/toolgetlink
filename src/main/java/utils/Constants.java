package utils;

public class Constants {
		public static final String CLOUD_GURU_USER_NAME = "viettn85@gmail.com";
		public static final String USER_NAME = "ngocle9185@gmail.com";
		public static final String PASSWORD = "study4ever";
		public static final String LIST_COURSE_URL_FILE_NAME = "listCourseURL.properties";
		public static final String LOGIN_URL = "https://app.pluralsight.com/id/";
		public static final int DEFAULT_TIMEOUT = 10;
		public static final String ROOT_PATH = "D:\\WorkSpace\\Study\\Ducky\\CloudGuru\\";
		
		public static final String XPATH_BUTTON_SIGN_IN = ".//div[@class='header_utilities']/a[contains(.,'Sign in')]";
		public static final String XPATH_COURSE_OVERVIEW = ".//a[@target='psplayer'][contains(.,'Course Overview')]/parent::h3/a";
		public static final String XPATH_VIDEO_TAG = ".//video[@id='vjs_video_3_html5_api']";
		public static final String XPATH_BUTTON_PAUSE = ".//button[@id='play-control']";
		public static final String XPATH_BUTTON_NEXT = ".//button[@id='next-control']";
		public static final String XPATH_COURSE_NAME = ".//a[@id='course-title-link']";
		public static final String XPATH_COURSE_SUB_FOLDER = ".//div[@id='module-clip-title']";
		public static final String XPATH_COURSE_VIDEO_TITLE = ".//span[@id='clip-title']";
		
		public static final String XPATH_BUTTON_CLOSE_POPUP = ".//a[@class='close']";
		
}	
