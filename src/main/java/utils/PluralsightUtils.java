package utils;

import static utils.Constants.PASSWORD;
import static utils.Constants.USER_NAME;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PluralsightUtils extends BasePage {
	static int countDuplicate = 0;
	static String coursename = "";
	static int index = 0;

	public PluralsightUtils(WebDriver driver, WebDriverWait driverWait,
			Actions actions) {
		super(driver, driverWait, actions);
		// TODO Auto-generated constructor stub
	}

	public void closeAds() {
		try {
			int size = driver.findElements(By.tagName("iframe")).size();
			System.out.println("number of iframe: " + size);
			if (size == 3) {
				countDuplicate = 0;
				return;

			}
			driver.switchTo().defaultContent();
			sleep(1);
			driver.switchTo()
					.frame(waiUtilVisible(
							By.xpath(".//iframe[@id='webklipper-publisher-widget-container-notification-frame']"))
							.getAttribute("id"));

			myClick(By.xpath(Constants.XPATH_BUTTON_CLOSE_POPUP));
			driver.switchTo().defaultContent();
		} catch (NoSuchElementException e) {
			return;
		}
	}

	public static void downloadFile(String fileURL, String saveDir,
			String fileName) throws IOException {
		URL url = new URL(fileURL);
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		int responseCode = httpConn.getResponseCode();

		// always check HTTP response code first
		if (responseCode == HttpURLConnection.HTTP_OK) {
			String contentType = httpConn.getContentType();
			int contentLength = httpConn.getContentLength();
			if (isDuplicateFile(saveDir, fileName, contentLength)) {
				httpConn.disconnect();
				countDuplicate++;
				index--;
				return;
			} else {
				countDuplicate = 0;
			}
			saveCurrentFileDownloadInformation(saveDir, contentType, fileName,
					contentLength);
			System.out.println("Content-Type = " + contentType);
			System.out.println("fileName = " + fileName);
			System.out.println("Content-Length = " + contentLength);
			File directory = new File(String.valueOf(saveDir));
			if (!directory.exists()) {
				directory.mkdirs();
			}
			// opens input stream from the HTTP connection
			InputStream inputStream = httpConn.getInputStream();
			String saveFilePath = saveDir + File.separator + fileName;

			// opens an output stream to save into file
			FileOutputStream outputStream = new FileOutputStream(saveFilePath);
			int bytesRead = -1;
			byte[] buffer = new byte[1024 * 1024];
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			outputStream.close();
			inputStream.close();
			System.out.println("File downloaded");
		} else {
			System.out
					.println("No file to download. Server replied HTTP code: "
							+ responseCode);
		}
		httpConn.disconnect();
	}

	public static void saveCurrentFileDownloadInformation(String savePath,
			String contentType, String fileName, int contentLength) {
		try {
			PrintWriter writer = new PrintWriter(savePath + "\\temp.txt",
					"UTF-8");
			writer.println("contentType=" + contentType);
			writer.println("fileName=" + fileName);
			writer.println("contentLength=" + contentLength);
			writer.close();
		} catch (IOException e) {
			// do something
		}
	}

	public static List<String> getCurrentDownloadFileInformation(String savePath) {
		List<String> fileInformation = new ArrayList<String>();
		try {
			File file = new File(savePath + "\\temp.txt");
			if (!file.exists()) {
				return fileInformation;
			}
			FileReader reader = new FileReader(savePath + "\\temp.txt");
			BufferedReader bufferedReader = new BufferedReader(reader);
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				fileInformation.add(line);
			}
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileInformation;
	}

	public static boolean isDuplicateFile(String savePath, String newFileName,
			int newContentLength) {
		List<String> fileInformation = new ArrayList<String>();
		fileInformation = getCurrentDownloadFileInformation(savePath);
		boolean isDuplicate = false;
		for (String infor : fileInformation) {
			if (infor.contains("fileName") && infor.contains(newFileName)) {
				isDuplicate = true;
			} else if (infor.contains("contentLength")
					&& infor.contains(newContentLength + "")) {
				isDuplicate = true;
			}
		}
		return isDuplicate;
	}

	public List<String> getListCourseURL(String fileName) {
		List<String> listCourseURL = new ArrayList<String>();
		// Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				listCourseURL.add(line);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listCourseURL;
	}

	public void doLogin() {
		By btnSigninLocator = By.xpath(Constants.XPATH_BUTTON_SIGN_IN);
		By emailLocator = By.xpath(".//input[@id='Username']");
		By passwordLocator = By.xpath(".//input[@id='Password']");
		By btnOKLocator = By.xpath(".//button[@id='login']");

		myClick(btnSigninLocator);
		mySendKey(emailLocator, USER_NAME);
		mySendKey(passwordLocator, PASSWORD);
		myClick(btnOKLocator);
		sleep(2);
	}

	public void gotoCourse() {
		By firstLink = By
				.xpath("(.//h3[@class='table-of-contents__title']/a)[1]");
		myClick(firstLink);
		sleep(2);
	}

	public void switchTab() {
		ArrayList<String> tabs2 = new ArrayList<String>(
				driver.getWindowHandles());
		if (tabs2.size() < 2)
			return;

		driver.switchTo().window(tabs2.get(0));
		driver.close();
		driver.switchTo().window(tabs2.get(1));
		sleep(3);
	}

	public void doGetLink(String courseURL) {
		driver.get(courseURL);
		// driver.manage().window().maximize();
		doLogin();
		gotoCourse();
		switchTab();
		index = 0;
		By btnNextLocator = By.xpath(Constants.XPATH_BUTTON_NEXT);
		By btnPauseLocator = By.xpath(Constants.XPATH_BUTTON_PAUSE);
		closeAdsAndClick(btnPauseLocator);
		getVideoUrlAndDownload();
		while (true) {
			index++;
			closeAdsAndClick(btnNextLocator);
			sleep(10);
			closeAdsAndClick(btnPauseLocator);
			getVideoUrlAndDownload();
			if (countDuplicate > 1) {
				System.err.println("Ads occurred");
				closeAds();
			}
			if (countDuplicate > 2) {
				System.err.println("Duplicated 2 times");
				countDuplicate = -1;
				return;
			}
		}
	}

	private void closeAdsAndClick(By btnNextLocator) {
		try {
			myClick(btnNextLocator);

		} catch (WebDriverException e) {
			closeAds();
			sleep(2);
			myClick(btnNextLocator);
		}
	}


	public void getVideoUrlAndDownload() {

		WebElement videotag = waiUtilVisible(By
				.xpath(Constants.XPATH_VIDEO_TAG));
		if (!coursename
				.equals(myGetText(By.xpath(Constants.XPATH_COURSE_NAME)))
				&& !myGetText(By.xpath(Constants.XPATH_COURSE_NAME)).isEmpty()) {
			coursename = myGetText(By.xpath(Constants.XPATH_COURSE_NAME));

		}

		String clipTitle = myGetText(By
				.xpath(Constants.XPATH_COURSE_SUB_FOLDER));
		if (clipTitle.indexOf(":") == -1) {
			System.err.println("clipTitle is null or empty, clipTitle: "
					+ clipTitle);
			index--;
			return;
		}

		String subFolder = clipTitle.substring(0, clipTitle.indexOf(":") - 1);
		String videoName = index
				+ ". "
				+ clipTitle.replaceAll("[^a-zA-Z0-9.-]", "_").substring(
						clipTitle.indexOf(":") + 1) + ".mp4";
		String srcVideo = videotag.getAttribute("src");

		System.out.println("coursename: " + coursename + ", subFolder: "
				+ subFolder + ", videoName: " + videoName + ", srcVideo: "
				+ srcVideo);
		String savePath = Constants.ROOT_PATH + coursename + "\\" + subFolder;
		System.out.println("savePath: " + savePath);
		try {
			downloadFile(srcVideo, savePath, videoName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
